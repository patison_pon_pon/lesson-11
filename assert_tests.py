lowercase_letters = tuple("abcdefghijklmnopqrstuvwxyz")
uppercase_letters = tuple("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
digits = tuple("0123456789")
symbols = tuple("*-#")


def check_password(password):
    symbol_count = 0
    has_lowercase = False
    has_uppercase = False
    has_digits = False
    has_symbols = False

    for symbol in password:
        symbol_count += 1
        if symbol in lowercase_letters:
            has_lowercase = True

        if symbol in uppercase_letters:
            has_uppercase = True

        if symbol in digits:
            has_digits = True

        if symbol in symbols:
            has_symbols = True

    if symbol_count < 8:
        return 'Длина пароля меньше 8 символов'
    if symbol_count > 12:
        return 'Длина пароля больше 12 символов'
    if not has_lowercase:
        return 'В пароле должны быть буквы в нижнем регистре'
    if not has_uppercase:
        return 'В пароле должны быть буквы в верхнем регистре'
    if not has_digits:
        return 'В пароле должны быть цифры'
    if not has_symbols:
        return f'В пароле должны быть хотя бы один из символов {symbols}'

    return 'Пароль идеален'


if __name__ == '__main__':
    assert check_password('aaa') == 'Длина пароля меньше 8 символов'
    assert check_password('aaaaaaaaaaaaaaa') == 'Длина пароля больше 12 символов'
    assert check_password('AAAAAAAAA') == 'В пароле должны быть буквы в нижнем регистре'
    assert check_password('aaaaaaaaa') == 'В пароле должны быть буквы в верхнем регистре'
    assert check_password('aaaaaaAAA') == 'В пароле должны быть цифры'
    assert check_password('111aaaAAA') == f'В пароле должны быть хотя бы один из символов {symbols}'
    assert check_password('111a*aAAA') == 'Пароль идеален'
