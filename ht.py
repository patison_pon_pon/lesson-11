from faker import Faker
import random

faker = Faker("ru_RU")


class Human:
    def __init__(self, first_name="-", last_name="-"):
        self.first_name = first_name
        self.last_name = last_name
        self.address = faker.address()
        self.phone_number = faker.phone_number()
        self.job = faker.job()
        self.salary = random.randrange(10000, 50000, 10)

    def show_info(self):
        print(f"Имя: {self.first_name}\n"
              f"Фамилия: {self.last_name}\n"
              f"Адрес: {self.address}\n"
              f"Номер телефона: {self.phone_number}\n"
              f"Профессия: {self.job}\n"
              f"Зарплата: {self.salary}"
              )


human = Human()


class Man(Human):
    def __init__(self, first_name="-", last_name="-"):
        super().__init__(first_name, last_name)
        self.first_name = faker.first_name_male()
        self.last_name = faker.last_name_male()


man = Man()


class Woman(Human):
    def __init__(self, first_name="-", last_name="-"):
        super().__init__(first_name, last_name)
        self.first_name = faker.first_name_female()
        self.last_name = faker.last_name_female()


woman = Woman()

random_int = random.randint(0, 1)
print(random_int)
if random_int == 0:
    man.show_info()
else:
    woman.show_info()


a = Human()
a.show_info()
