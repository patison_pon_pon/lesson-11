from accessify import private, protected


class Parent:
    def __init__(self, number=100):
        self.number = 100
        if self.__check_value(number):
            self.number = number
        self._number = self.number * 2
        self.__number = self.number * 3

    def print_public(self):
        print(self.number)

    def print_protected(self):
        print(self._number)

    def print_private(self):
        # print(self.__number)
        self.test_private()

    @staticmethod
    def __check_value(number):
        return type(number) in (int, float)

    @private
    def test_private(self):
        print(self.__number)

    @protected
    def test_protected(self):
        print(self._number)

    def change_private(self, number):
        # if type(number) in (int, float):
        if self.__check_value(number):
            self.__number = number
        else:
            raise ValueError('Введите число')


class Child(Parent):
    pass

    # def change_private(self, number):
    #     self.__number = number

    # def print_private(self):
    #     print(self.__number)


parent = Parent()
print(parent.number)
parent.number = 400
print(parent.number)
print(parent._number)
# print(parent.__number)
parent.print_public()
parent.print_protected()
parent.print_private()
parent.change_private(500)
parent.print_private()
# print(dir(parent))
# print(parent._Parent__number)
# print(parent.test_private())
print('----------------------------------------------')

child = Child()
print(child.number)
child.number = 400
print(child.number)
print(child._number)
# print(child.__number)
child.print_public()
child.print_protected()
# child.print_private()
# child.change_private(500)
# child.print_private()



