from typing import List, Dict, Optional, Union

first = 100
print(type(first))

first = 'hello'
print(type(first))

first: int = 'hello'
print(type(first))

first: int = 100
print(type(first))


def add_numbers(a: int, b: int) -> int:
    return a + b


add_numbers(100, 100)
add_numbers('hello', 'world')
add_numbers([1, 2, 3], [4, 5, 6])
# add_numbers(100, [1, 2, 3])

print(add_numbers.__annotations__)


def list_upper(lst: List[str]):
    for value in lst:
        print(value.upper())


list_upper(['a', 'b', 'c'])
# list_upper(['a', 'b', 'c', 1])


def add_numbers(a: Union[int, float], b: Optional[int] = None) -> int:
    if b is None:
        return a ** 5
    return a + b


print(add_numbers(25.42))


# def add_numbers(a: int | float | str, b: int | float | str) -> int:
#     return a + b
